package com.galvarez.telephony.activity;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.galvarez.telephony.R;
import com.galvarez.telephony.fragment.RuleDetailFragment;

public class RuleActivity extends AppCompatActivity {

    private final static String Tag = RuleActivity.class.getName();

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rule);
        fragment =  getFragmentManager().findFragmentById(R.id.fragment);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (ContactsContract.Intents.SEARCH_SUGGESTION_CLICKED.equals(intent.getAction())) {
            //handles suggestion clicked query

            String[] detail= getDisplayNameForContact(intent);
            ((RuleDetailFragment)fragment).updateListView(detail[0], detail[1]);

        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query

            String query = intent.getStringExtra(SearchManager.QUERY);
            ((RuleDetailFragment)fragment).updateListView("Something","Wrong");
        }
    }

    private String[] getDisplayNameForContact(Intent intent) {

        String[] detail = new String[2];
        Cursor phoneCursor = getContentResolver().query(intent.getData(), null, null, null, null);
        phoneCursor.moveToFirst();

        int idDisplayName = phoneCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        detail[0]= phoneCursor.getString(idDisplayName);

        String id = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.Contacts._ID));
        Cursor pCur = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                new String[]{id},
                null);

        while(pCur.moveToNext()){
            detail[1] =pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

//        int idNumber      = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
//        detail[1]= phoneCursor.getString(idNumber);

        if(detail[1] == null){
            detail[1] = "-1";
        }

//        Log.e(Tag,"getDisplayNameForContact() Name:" + detail[0]);
//        Log.e(Tag,"getDisplayNameForContact() Number:" + detail[1]);

        phoneCursor.close();
        return detail;
    }
}
