package com.galvarez.telephony.broadcastreceiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.galvarez.telephony.database.RuleTable;
import com.galvarez.telephony.database.RuleTableProvider;

import java.util.Calendar;

public class PhoneCallReceiver extends BroadcastReceiver {

    private static final String Tag = "Phone call";
    private final static boolean d_ruleApply = true;
    private final static boolean d_compareNumber = true;
    private final static boolean d_isRuleEnabled = true;
//    private ITelephony telephonyService;

    private final static String PREFERENCE = "SOUNDPREFS";
    private final static String PREFERENCE_VOLUME = "SOUNDPREFSVOLUME";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(Tag, "Receiving....");

//        TelephonyManager telephony = (TelephonyManager)
//                context.getSystemService(Context.TELEPHONY_SERVICE);

        if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            // This code will execute when the phone has an incoming call
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            Toast.makeText(context, "Call from: " +incomingNumber, Toast.LENGTH_LONG).show();

            ruleApply(context, queryData(context, RuleTableProvider.CONTENT_URI), incomingNumber);

        } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                TelephonyManager.EXTRA_STATE_IDLE)
                || intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                TelephonyManager.EXTRA_STATE_OFFHOOK)) {

            SharedPreferences prefs = context.getSharedPreferences(PREFERENCE, Activity.MODE_PRIVATE);
            int oldVolume = prefs.getInt(PREFERENCE_VOLUME, -1);

            /** if there is an old volume number saved it means we are coming back from a call */
            if(oldVolume != -1){

                AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                audioManager.setStreamVolume(AudioManager.STREAM_RING, oldVolume, AudioManager.FLAG_ALLOW_RINGER_MODES| AudioManager.FLAG_PLAY_SOUND);

                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(PREFERENCE_VOLUME, -1);
                edit.apply();
            }
        }

//        TelephonyManager telephony = (TelephonyManager)
//                context.getSystemService(Context.TELEPHONY_SERVICE);
        /*try {
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            //telephonyService.silenceRinger();
            telephonyService.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private static  boolean ruleApply(Context context, Cursor cursor, String incoming){

        boolean foundMatch = false;

        String phoneAsString;
        String[] phonesAsArray;

        if (cursor != null) {
            if(d_ruleApply)Log.e(Tag,"ruleApply() Found " + cursor.getCount() + " entries");

            if(cursor.getCount() > 0){

                //STEP 1 Find out if the number exists in any rules
                cursor.moveToFirst();
                phoneAsString = cursor.getString(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_NUMBER));
                phonesAsArray =  phoneAsString.split(",");

                for(String number: phonesAsArray){
                    if(compareNumbers(number, incoming)){
                        foundMatch = true;
                        break;
                    }
                }

                if(!foundMatch){
                    while(cursor.moveToNext()){
                        phoneAsString = cursor.getString(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_NUMBER));
                        phonesAsArray =  phoneAsString.split(",");

                        for(String number: phonesAsArray){
//                        if(d_ruleApply) Log.e(Tag, compareNumbers(number, incoming) ? "Match":"No Match");
                            if(compareNumbers(number, incoming)){
                                if(d_ruleApply) Log.e(Tag, compareNumbers(number, incoming) ? "Match":"No Match");
                                foundMatch = true;
                                break;
                            }
                        }
                        if(foundMatch) {
                            Log.e(Tag,"Breaking cursor loop");
                            break;}
                    }
                }

                //STEP 2 Find out if this rule should apply right now
                //Is the rule on ?
                if(foundMatch){
                    foundMatch = isRuleEnabled(cursor);
                }

                //Is it the right day ?
                if(foundMatch) {
                    if (doesRuleApplyToday(cursor) && doesRuleApplyNow(cursor)) {
                        //Raise the volumes
                        Log.e(Tag, "Raising Volumes");

                        //First save the volume into memory so we can restore it when we finish the call
                        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE, Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();

                        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                        int streamMaxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);

                        editor.putInt(PREFERENCE_VOLUME, audioManager.getStreamVolume(AudioManager.STREAM_RING));
                        editor.apply();

                        audioManager.setStreamVolume(AudioManager.STREAM_RING, streamMaxVolume, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);
                    }
                }

                //Is it the right time?
                cursor.close();
            }

            return  false;

        }else{
            if(d_ruleApply)Log.e(Tag,"ruleApply() Null Cursor");
            return false;
        }
    }

    private static boolean isRuleEnabled(Cursor cursor){

        boolean ret = cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ISENABLED)) == 1;
        if(d_isRuleEnabled)Log.e(Tag,"RuleEnabled: " + ret);
        return ret;
    }

    private static boolean doesRuleApplyToday(Cursor cursor){
        Calendar rightNow = Calendar.getInstance();
        switch (rightNow.get(Calendar.DAY_OF_WEEK)){
            case Calendar.SUNDAY:   return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_SUN)) == 1;
            case Calendar.MONDAY:   return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_MON)) == 1;
            case Calendar.TUESDAY:  return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_TUE)) == 1;
            case Calendar.WEDNESDAY:return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_WED)) == 1;
            case Calendar.THURSDAY: return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_THU)) == 1;
            case Calendar.FRIDAY:   return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_FRI)) == 1;
            case Calendar.SATURDAY: return cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_SAT)) == 1;
            default:return false;
        }
    }

    private static boolean doesRuleApplyNow(Cursor cursor){

        Calendar rightNow = Calendar.getInstance();
        Calendar startTime = Calendar.getInstance();
        Calendar endTime = Calendar.getInstance();

        long rightNowMillis = rightNow.getTimeInMillis();

        startTime.set(Calendar.HOUR_OF_DAY,   cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_STARTTIME_HOUR)));
        startTime.set(Calendar.MINUTE, cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_STARTTIME_MINUTE)));

        endTime.set(Calendar.HOUR_OF_DAY, cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ENDTIME_HOUR)));
        endTime.set(Calendar.MINUTE, cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ENDTIME_MINUTE)));

       /* Log.e(Tag,"TIME RIGHT NOW: " +rightNowMillis );
        Log.e(Tag,"Start time: " +startTime.getTimeInMillis() );
        Log.e(Tag, "end time : " + endTime.getTimeInMillis());*/

        return (rightNowMillis >= startTime.getTimeInMillis() && rightNowMillis <= endTime.getTimeInMillis());
    }

    private static boolean compareNumbers(String number1, String number2){

        String A = number1;
        String B = number2;

        //Strip the number down to 10 digits with no crazy characters
        if(A.length() > 10){

            if(d_compareNumber)Log.e(Tag,"Parsing A");
            A = A.trim();
            A = A.replace(" ","");
            A = A.replace("(","");
            A = A.replace(")","");
            A = A.replace("+","");
            A = A.replace("-","");
        }

        if(B.length() > 10){
            if(d_compareNumber)Log.e(Tag,"Parsing B");
            B = B.trim();
            B = B.replace(" ","");
            B = B.replace("(","");
            B = B.replace(")","");
            B = B.replace("+","");
            B = B.replace("-","");
        }

        return A.contains(B);
    }

    private Cursor queryData(Context context, Uri uri) {

        /** Indicate which tbles we want to pull fromm */
        String[] projection = {
                RuleTable.COLUMN_LABEL,
                RuleTable.COLUMN_STARTTIME_HOUR,
                RuleTable.COLUMN_STARTTIME_MINUTE,
                RuleTable.COLUMN_ENDTIME_HOUR,
                RuleTable.COLUMN_ENDTIME_MINUTE,
                RuleTable.COLUMN_SUN,
                RuleTable.COLUMN_MON,
                RuleTable.COLUMN_TUE,
                RuleTable.COLUMN_WED,
                RuleTable.COLUMN_THU,
                RuleTable.COLUMN_FRI,
                RuleTable.COLUMN_SAT,
                RuleTable.COLUMN_FULL_NAME,
                RuleTable.COLUMN_NUMBER,
                RuleTable.COLUMN_ISENABLED
        };
        /** we make a cursor with the query*/
       return  context.getContentResolver().query(
                uri,
                projection,
                null,
                null,
                null);

    }
}
