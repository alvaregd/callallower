package com.galvarez.telephony.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by alvaregd on 01/07/15.
 */
public class RuleTableProvider extends ContentProvider {


    // database
    private RuleTableBaseHelper database;

    // used for the UriMacher
    private static final int RULES = 10;
    private static final int RULES_ID = 20;

    private static final String AUTHORITY = "com.galvarez.telephony.database";

    private static final String BASE_PATH = "rules";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/rules";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/rule";

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, RULES);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", RULES_ID);
    }

    @Override
    public boolean onCreate() {
        database = new RuleTableBaseHelper(getContext());
        return false;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // Set the table
        queryBuilder.setTables(RuleTable.TABLE_RULE);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case RULES:
                break;
            case RULES_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(RuleTable.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        long id = 0;
        switch (uriType) {
            case RULES:
                id = sqlDB.insert(RuleTable.TABLE_RULE, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case RULES:
                rowsDeleted = sqlDB.delete(RuleTable.TABLE_RULE, selection,
                        selectionArgs);
                break;
            case RULES_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(RuleTable.TABLE_RULE,
                            RuleTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(RuleTable.TABLE_RULE,
                            RuleTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case RULES:
                rowsUpdated = sqlDB.update(RuleTable.TABLE_RULE,
                        values,
                        selection,
                        selectionArgs);
                break;
            case RULES_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(RuleTable.TABLE_RULE,
                            values,
                            RuleTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(RuleTable.TABLE_RULE,
                            values,
                            RuleTable.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = {
                RuleTable.COLUMN_ID,
                RuleTable.COLUMN_LABEL,
                RuleTable.COLUMN_STARTTIME_HOUR,
                RuleTable.COLUMN_STARTTIME_MINUTE,
                RuleTable.COLUMN_ENDTIME_HOUR,
                RuleTable.COLUMN_ENDTIME_MINUTE,
                RuleTable.COLUMN_SUN,
                RuleTable.COLUMN_MON,
                RuleTable.COLUMN_TUE,
                RuleTable.COLUMN_WED,
                RuleTable.COLUMN_THU,
                RuleTable.COLUMN_FRI,
                RuleTable.COLUMN_SAT,
                RuleTable.COLUMN_FULL_NAME,
                RuleTable.COLUMN_NUMBER,
                RuleTable.COLUMN_ISENABLED
        };
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }









}
