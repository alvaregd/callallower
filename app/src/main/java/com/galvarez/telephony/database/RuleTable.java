package com.galvarez.telephony.database;


import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RuleTable {

    // Database table
    public static final String TABLE_RULE = "rules";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LABEL     ="label";
    public static final String COLUMN_STARTTIME_HOUR = "startTimeHour";
    public static final String COLUMN_STARTTIME_MINUTE = "startTimeMinute";
    public static final String COLUMN_ENDTIME_HOUR = "endTimedHour";
    public static final String COLUMN_ENDTIME_MINUTE = "endTimeMinute";

    public static final String COLUMN_SUN = "sunday";
    public static final String COLUMN_MON = "monday";
    public static final String COLUMN_TUE = "tuesday";
    public static final String COLUMN_WED = "wednesday";
    public static final String COLUMN_THU = "thursday";
    public static final String COLUMN_FRI = "friday";
    public static final String COLUMN_SAT = "saturday";

    public static final String COLUMN_FULL_NAME = "fullName";
    public static final String COLUMN_NUMBER = "number";

    public static final String COLUMN_ISENABLED= "enabled";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_RULE
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_LABEL + " text not null, "
            + COLUMN_STARTTIME_HOUR + " integer not null, "
            + COLUMN_STARTTIME_MINUTE + " integer not null, "
            + COLUMN_ENDTIME_HOUR + " integer not null, "
            + COLUMN_ENDTIME_MINUTE + " integer not null, "

            + COLUMN_SUN + " integer not null,"
            + COLUMN_MON + " integer not null,"
            + COLUMN_TUE + " integer not null,"
            + COLUMN_WED + " integer not null,"
            + COLUMN_THU + " integer not null,"
            + COLUMN_FRI + " integer not null,"
            + COLUMN_SAT + " integer not null,"
            + COLUMN_FULL_NAME + " text not null,"
            + COLUMN_NUMBER + " text not null,"
            + COLUMN_ISENABLED + " integer not null "
            + ");";


    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(RuleTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_RULE);
        onCreate(database);
    }

}
