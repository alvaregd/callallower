package com.galvarez.telephony.views;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.galvarez.telephony.R;
import com.galvarez.telephony.activity.RuleActivity;

import org.w3c.dom.Text;

public class BottomActionBar extends LinearLayout implements View.OnClickListener {

    Context context;
    TextView new_rule;

    public BottomActionBar(Context context) {
        super(context);
        init(context);
    }

    public BottomActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BottomActionBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context){
        this.context = context;
        LayoutInflater lif = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lif.inflate(R.layout.bottom_action_bar,this);
        new_rule = (TextView)this.findViewById(R.id.tv_new_rule);
//        new_rule = (ImageView)this.findViewById(R.id.iv_new_rule);
        new_rule.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_new_rule:
                //TODO start a new activity
                context.startActivity(new Intent(context, RuleActivity.class));
                Toast.makeText(context,"New Rule",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
