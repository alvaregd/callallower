package com.galvarez.telephony.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by alvaregd on 01/07/15.
 */
public class NonEnterEditText  extends EditText {

    private Context context;

    public NonEnterEditText(Context context) {
        super(context);
        init(context);
    }

    public NonEnterEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public NonEnterEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        this.context =context;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode==KeyEvent.KEYCODE_ENTER)
        {
            // Just ignore the [Enter] key
            InputMethodManager imm  = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getWindowToken(),0);
            this.clearFocus();
            return true;
        }
        // Handle all other keys in the default way
        return super.onKeyDown(keyCode, event);
    }
}