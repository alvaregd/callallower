package com.galvarez.telephony.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.galvarez.telephony.R;
import com.galvarez.telephony.activity.RuleActivity;

public class RuleDeleteButton extends LinearLayout implements View.OnClickListener{

    Context context;
    TextView delete;

    public RuleDeleteButton(Context context) {
        super(context);
        init(context);
    }

    public RuleDeleteButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RuleDeleteButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        this.context = context;
        LayoutInflater lif = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lif.inflate(R.layout.delete_button,this);
        delete = (TextView)this.findViewById(R.id.b_delete);
        delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(context, "Deleted",Toast.LENGTH_SHORT).show();
        ((RuleActivity)context).finish();
    }
}
