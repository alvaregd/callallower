package com.galvarez.telephony.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.galvarez.telephony.R;

public class RuleListAdapter extends ArrayAdapter<String> {

    Context context;
    GradientDrawable gradientDrawable;

    public RuleListAdapter(Context context) {
        super(context, R.layout.entry_rule_list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_rule_list,parent, false);
        }

        TextView tv = (TextView)convertView.findViewById(R.id.tv_label_in_list);
        tv.setText("Super Long label " + position);

        View ll_beam = convertView.findViewById(R.id.color_board);
        gradientDrawable = (GradientDrawable)ll_beam.getBackground();
        gradientDrawable.setColor(Color.GRAY);

        return convertView;
    }


    @Override
    public int getCount() {
        return super.getCount();
    }
}
