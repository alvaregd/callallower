package com.galvarez.telephony.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.galvarez.telephony.R;
import com.galvarez.telephony.database.RuleTable;
import com.galvarez.telephony.database.RuleTableProvider;
import com.galvarez.telephony.fragment.RuleListFragment;

import java.util.Calendar;

/** your first cursor adapter, therefor we take notes
 * this adapter is used to populate the contents of the
 * rule list fragment*/
public class RuleCursorAdapter extends SimpleCursorAdapter  {

    private final static String Tag= "RuleCursorAdapter";
    private final static boolean d_onBind = false;

    private Context context;
    private Context appContext;
    private Cursor cursor;
    private RuleCursorAdapter self;

    private Cursor newestCursor;

    public RuleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);

        this.context = context;
        this.cursor = c;
        this.self = this;
    }


    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {

//        if(d_onBind)Log.e(Tag,"onBind()" );

//        newestCursor = cursor;

        if(d_onBind)Log.e(Tag,"Updating for:" + cursor.getLong(cursor.getColumnIndex("_id")));

        StringBuilder builder = new StringBuilder();
        boolean hasOneAtEnd = false;

        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_SUN)) == 1){
            builder.append("SUN");
            hasOneAtEnd = true;
        }

        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_MON)) == 1){

            if(hasOneAtEnd){
                hasOneAtEnd = false;
                builder.append(", ");
            }

            builder.append("MON");
            hasOneAtEnd = true;
        }

        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_TUE)) == 1){

            if(hasOneAtEnd){
                hasOneAtEnd = false;
                builder.append(", ");
            }
            builder.append("TUE");
            hasOneAtEnd = true;
        }


        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_WED)) == 1){
            if(hasOneAtEnd){
                hasOneAtEnd = false;
                builder.append(", ");
            }
            builder.append("WED");
            hasOneAtEnd = true;
        }


        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_THU)) == 1){
            if(hasOneAtEnd){
                hasOneAtEnd = false;
                builder.append(", ");
            }
            builder.append("THU");
            hasOneAtEnd = true;
        }

        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_FRI)) == 1){
            if(hasOneAtEnd){
                hasOneAtEnd = false;
                builder.append(", ");
            }
            builder.append("FRI");
            hasOneAtEnd = true;
        }
        if(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_SAT)) == 1){
            if(hasOneAtEnd){
                hasOneAtEnd = false;
                builder.append(", ");
            }
            builder.append("SAT");
            hasOneAtEnd = true;
        }

        TextView daysActive = (TextView)view.findViewById(R.id.tv_days_active);
        daysActive.setText(builder.toString());

        Switch enable = (Switch)view.findViewById(R.id.on_off_switch);

        enable.setOnCheckedChangeListener(null);
        enable.setChecked(cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ISENABLED)) == 1);
        enable.setOnCheckedChangeListener(getNewlistener(cursor.getLong(cursor.getColumnIndex("_id"))));

        super.bindView(view, context, cursor);
    }


    public CompoundButton.OnCheckedChangeListener getNewlistener(final long id) {

        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                ContentValues values = new ContentValues();
                values.put(RuleTable.COLUMN_ISENABLED, isChecked ? 1 : 0);
                Uri uri = Uri.parse(RuleTableProvider.CONTENT_URI + "/" +id);
                if(d_onBind)Log.e(Tag,"Updating for:" + uri.toString() + " turned " + isChecked);
                context.getContentResolver().update(uri, values, null, null);
            }
        };
    }
}
