package com.galvarez.telephony.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.galvarez.telephony.R;
import com.galvarez.telephony.data.NameNumberPair;
import com.galvarez.telephony.database.RuleTable;
import com.galvarez.telephony.database.RuleTableProvider;
import com.galvarez.telephony.views.NonEnterEditText;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**Allows us to create or edit a rule */

public class RuleDetailFragment extends Fragment implements View.OnClickListener {

    private final static String Tag = RuleDetailFragment.class.getName();
    private final static boolean d_onCreate = false;
    private final static boolean d_onPause = false;
    private final static boolean d_onActivityResult = false;
    private final static boolean d_onOptionsitemSelected = false;
    private final static boolean d_fillData = false;

    private final static String SEARCH_TAG = "SEARCHTAG";

    /** UI STUFF **/

    private View rootView;
    private TextView timeStart;
    private TextView timeEnd;

    private boolean SUN = false;  // 0 is false, 1 is true
    private boolean MON = false;
    private boolean TUE = false;
    private boolean WED = false;
    private boolean THU = false;
    private boolean FRI = false;
    private boolean SAT = false;

    private TextView tvSUN, tvMON,tvTUE,tvWED,tvTHU,tvFRI,tvSAT;
    private NonEnterEditText label;
    private int startHour;
    private int endHour;
    private int startMinute;
    private int endMinute;
    private LinearLayout nameList;
    private SearchView search;

    private HashMap<ImageView, NameNumberPair> inMemoryContacts = new HashMap<>();

    /** Cursor Stuff **/
    private Uri ruleUri;


    public RuleDetailFragment() {

    }

    interface TimePickerCallBack{
        void setTime(int hour, int minute);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(d_onCreate)Log.e(RuleDetailFragment.class.getName(),"onCreate()");
    }

    @Override
    public void onPause() {
        super.onPause();
        if(d_onPause)Log.e(Tag,"onPause()");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(d_onActivityResult)Log.e(Tag,"requestCode:" + requestCode);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /** save the details */
        saveState();
        /** save the uri of our query so we can use it to refetch data **/
        outState.putParcelable(RuleTableProvider.CONTENT_ITEM_TYPE, ruleUri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_rule,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(d_onOptionsitemSelected)Log.e(RuleDetailFragment.class.getName(),"Enter Options Item Selected");

        Calendar startTime = Calendar.getInstance();
        Calendar endTime = Calendar.getInstance();

        startTime.set(Calendar.HOUR_OF_DAY, startHour);
        startTime.set(Calendar.MINUTE, startMinute);
        endTime.set(Calendar.HOUR_OF_DAY, endHour);
        endTime.set(Calendar.MINUTE, endMinute);

        switch (item.getItemId()){
            case R.id.action_save:

                if(label.getText().toString().isEmpty()){
                    makeToast("Label cannot be Empty");
                    makeToast("Select a End Time");
                }
                else if(timeStart.getText().toString().isEmpty()){
                    timeStart.setError("Must Pick a Time");
                    makeToast("Select a Start Time");
                }
                else if(timeEnd.getText().toString().isEmpty()){
                    timeEnd.setError("Must Pick a Time");
                    makeToast("Select a End Time");
                }else if (startTime.getTimeInMillis() > endTime.getTimeInMillis()){
                    timeStart.setError("Must be less than End Time");
                    makeToast("Must pick a start time less than the End time");
                }

                else{
                    getActivity().setResult(Activity.RESULT_OK);
                    saveState();
                    getActivity().finish();
                }
              return  true;
            default: return  false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_rule, container, false);
        }

        label = (NonEnterEditText)rootView.findViewById(R.id.et_label);
        label.setFocusableInTouchMode(true);

        timeStart = (TextView)rootView.findViewById(R.id.tv_time_start);
        timeStart.setOnClickListener(this);
        timeEnd = (TextView)rootView.findViewById(R.id.tv_time_end);
        timeEnd.setOnClickListener(this);

        tvSUN = (TextView)rootView.findViewById(R.id.tv_SUN);
        tvSUN.setOnClickListener(this);
        tvMON = (TextView)rootView.findViewById(R.id.tv_MON);
        tvMON.setOnClickListener(this);
        tvTUE = (TextView)rootView.findViewById(R.id.tv_TUE);
        tvTUE.setOnClickListener(this);
        tvWED = (TextView)rootView.findViewById(R.id.tv_WED);
        tvWED.setOnClickListener(this);
        tvTHU = (TextView)rootView.findViewById(R.id.tv_THU);
        tvTHU.setOnClickListener(this);
        tvFRI = (TextView)rootView.findViewById(R.id.tv_FRI);
        tvFRI.setOnClickListener(this);
        tvSAT = (TextView)rootView.findViewById(R.id.tv_SAT);
        tvSAT.setOnClickListener(this);

        nameList = (LinearLayout)rootView.findViewById(R.id.ll_name_list);

        //Create a new linear layout
        LinearLayout llSearch = new LinearLayout(getActivity());
        llSearch.setTag(SEARCH_TAG);
        llSearch.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        llSearch.setOrientation(LinearLayout.HORIZONTAL);

        configureSearchView(llSearch);
        nameList.addView(llSearch);

        /** Change if we have saved URI from saved Instance **/
        ruleUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState
                .getParcelable(RuleTableProvider.CONTENT_ITEM_TYPE);

        /** Check if we have data from a seperate activity */
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            ruleUri = extras.getParcelable(RuleTableProvider.CONTENT_ITEM_TYPE);
            if(ruleUri == null)Log.e("NULL URI"," NULL URI");
            else{
                Log.e("URI",ruleUri.toString());
                fillData(ruleUri);
            }
        }

       if(d_onCreate) Log.e("rulelURI IS NULL? ",(ruleUri != null ) ? ruleUri.toString(): "Null URI");
        return rootView;
    }

    private void configureSearchView(LinearLayout parent){
        //create a new Edittext and add it to search
        search = new SearchView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.weight = 1;
        search.setLayoutParams(layoutParams);
        search.setGravity(Gravity.CENTER_VERTICAL);
        parent.addView(search);

        SearchManager searchManager = (SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo searchableInfo =searchManager.getSearchableInfo(getActivity().getComponentName());
        search.setSearchableInfo(searchableInfo);
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.tv_time_start:
                DialogFragment newFragmentstart = TimePickerFragment.newInstance(new TimePickerCallBack() {
                    @Override
                    public void setTime(int hour, int minute) {
                        startHour = hour;
                        startMinute = minute;
                        timeStart.setText(hour + ":" + (minute < 10 ?  ( " " + minute):minute));
                    }
                });

                newFragmentstart.show(getActivity().getFragmentManager(), "timePickerStart");
                break;
            case R.id.tv_time_end:
                DialogFragment newFragmentend = TimePickerFragment.newInstance(new TimePickerCallBack() {
                    @Override
                    public void setTime(int hour, int minute) {
                        endHour = hour;
                        endMinute = minute;
                        timeEnd.setText(hour + ":" + (minute < 10 ?  ( " " + minute):minute));
                    }
                });
                newFragmentend.show(getActivity().getFragmentManager(), "timePickerEnd");
                break;

            case R.id.tv_SUN:
                SUN = !SUN;
                tvSUN.setPaintFlags(SUN ?  tvSUN.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvSUN.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvSUN.setTypeface(null, SUN ? Typeface.BOLD: Typeface.NORMAL);
                break;

            case R.id.tv_MON:
                MON = !MON;
                tvMON.setPaintFlags(MON ?  tvMON.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvMON.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvMON.setTypeface(null, MON ? Typeface.BOLD: Typeface.NORMAL);
                break;
            case R.id.tv_TUE:
                TUE = !TUE;
                tvTUE.setPaintFlags(TUE ?  tvTUE.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvTUE.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvTUE.setTypeface(null, TUE ? Typeface.BOLD: Typeface.NORMAL);
                break;
            case R.id.tv_WED:
                WED = !WED;
                tvWED.setPaintFlags(WED ?  tvWED.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvWED.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvWED.setTypeface(null, WED ? Typeface.BOLD: Typeface.NORMAL);
                break;
            case R.id.tv_THU:
                THU = !THU;
                tvTHU.setPaintFlags(THU ?  tvTHU.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvTHU.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvTHU.setTypeface(null, THU ? Typeface.BOLD: Typeface.NORMAL);
                break;
            case R.id.tv_FRI:
                FRI = !FRI;
                tvFRI.setPaintFlags(FRI ?  tvFRI.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvFRI.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvFRI.setTypeface(null, FRI ? Typeface.BOLD: Typeface.NORMAL);
                break;
            case R.id.tv_SAT:
                SAT = !SAT;
                tvSAT.setPaintFlags(SAT ?  tvSAT.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                        tvSAT.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                tvSAT.setTypeface(null, SAT ? Typeface.BOLD: Typeface.NORMAL);
                break;
        }
    }

    /** When get information about the user, we make a new linear layout
     * and add the user to the parent linear layout. We do this because we can't use a listview
     * and we can't use a listview because the entire fragment is scrollable
     * @param name name of the person
     */
    public void updateListView(String name, String number){

        Log.e(Tag,"upateListView() adding: " + name + "number" + number);
        //Create new linear layout
        LinearLayout parentLayout = new LinearLayout(getActivity());
        parentLayout.setTag(SEARCH_TAG);
        parentLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        parentLayout.setOrientation(LinearLayout.HORIZONTAL);

        TextView fullName = new TextView(getActivity());
        fullName.setText(name);
        fullName.setTextSize(20);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.weight = 1;
        fullName.setLayoutParams(layoutParams);
        fullName.setGravity(Gravity.CENTER_VERTICAL);
        parentLayout.addView(fullName);

        //Add a Add button
        ImageView remove = new ImageView(getActivity());
        remove.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nameList.removeView((View)v.getParent());
                inMemoryContacts.remove(v);
            }
        });
        remove.setImageResource(R.drawable.remove_icon);
        remove.setPadding(5,5,5,5);
        parentLayout.addView(remove);

        NameNumberPair pair = new NameNumberPair();
        pair.name = name;
        pair.number = number;
        inMemoryContacts.put(remove,pair);

        nameList.addView(parentLayout);
        search.setQuery("",false);
    }

    private void fillData(Uri uri) {

        /** Indicate which tbles we want to pull fromm */
        String[] projection = {
                RuleTable.COLUMN_LABEL,
                RuleTable.COLUMN_STARTTIME_HOUR,
                RuleTable.COLUMN_STARTTIME_MINUTE,
                RuleTable.COLUMN_ENDTIME_HOUR,
                RuleTable.COLUMN_ENDTIME_MINUTE,
                RuleTable.COLUMN_SUN,
                RuleTable.COLUMN_MON,
                RuleTable.COLUMN_TUE,
                RuleTable.COLUMN_WED,
                RuleTable.COLUMN_THU,
                RuleTable.COLUMN_FRI,
                RuleTable.COLUMN_SAT,
                RuleTable.COLUMN_FULL_NAME,
                RuleTable.COLUMN_NUMBER,
                RuleTable.COLUMN_ISENABLED
        };

        /** we make a cursor with the query*/
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
//            Log.e("URI RESULT: ","" +);
            label.setText(cursor.getString(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_LABEL)));
            startHour = cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_STARTTIME_HOUR));
            startMinute = cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_STARTTIME_MINUTE));
            endHour = cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ENDTIME_HOUR));
            endMinute = cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ENDTIME_MINUTE));

            timeStart.setText(startHour + ":" + (startMinute < 10 ?  ( " " + startMinute):startMinute));
            timeEnd.setText(endHour + ":" + (endMinute < 10 ?  ( " " + endMinute):endMinute));

            SUN =  cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_SUN)) == 1;
            tvSUN.setPaintFlags(SUN ?  tvSUN.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvSUN.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvSUN.setTypeface(null, SUN ? Typeface.BOLD: Typeface.NORMAL);

            MON =  cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_MON)) == 1;
            tvMON.setPaintFlags(MON ?  tvMON.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvMON.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvMON.setTypeface(null, MON ? Typeface.BOLD: Typeface.NORMAL);

            TUE =  cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_TUE)) == 1;
            tvTUE.setPaintFlags(TUE ?  tvTUE.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvTUE.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvTUE.setTypeface(null, TUE ? Typeface.BOLD: Typeface.NORMAL);

            WED =  cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_WED)) == 1;
            tvWED.setPaintFlags(WED ?  tvWED.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvWED.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvWED.setTypeface(null, WED ? Typeface.BOLD: Typeface.NORMAL);

            THU =  cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_THU)) == 1;
            tvTHU.setPaintFlags(THU ?  tvTHU.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvTHU.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvTHU.setTypeface(null, THU ? Typeface.BOLD: Typeface.NORMAL);

            FRI =  cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_FRI)) == 1;
            tvFRI.setPaintFlags(FRI ?  tvFRI.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvFRI.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvFRI.setTypeface(null, FRI ? Typeface.BOLD: Typeface.NORMAL);

            SAT = cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_SAT)) == 1;
            tvSAT.setPaintFlags(SAT ?  tvSAT.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG:
                    tvSAT.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
            tvSAT.setTypeface(null, SAT ? Typeface.BOLD: Typeface.NORMAL);

            //get our names
            String namesAsString = cursor.getString(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_FULL_NAME));
            String[] namesAsArray =  namesAsString.split(",");

            String phoneAsString = cursor.getString(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_NUMBER));
            String[] phonesAsArray =  phoneAsString.split(",");

            //get our phone numbers
            Log.e(Tag,"ArraySize: " + namesAsArray.length);

            for(int i =0; i < namesAsArray.length; i++){

                if(!namesAsArray[i].isEmpty() ||phonesAsArray[i].isEmpty() ){
                    updateListView(namesAsArray[i],phonesAsArray[i]);
                }
            }

            if(d_fillData)Log.e(Tag,"Enabled: "+ (cursor.getInt(cursor.getColumnIndexOrThrow(RuleTable.COLUMN_ISENABLED)) == 1));

            /** close the cursor when we finish */
            cursor.close();
        }
    }

    private void  saveState() {

        String label = this.label.getText().toString();

        // only save if label is avail
        if (label.length() == 0) {
            return;
        }

        ContentValues values = new ContentValues();
        values.put(RuleTable.COLUMN_LABEL, label);
        values.put(RuleTable.COLUMN_STARTTIME_HOUR, startHour);
        values.put(RuleTable.COLUMN_ENDTIME_HOUR, endHour);
        values.put(RuleTable.COLUMN_STARTTIME_MINUTE, startMinute);
        values.put(RuleTable.COLUMN_ENDTIME_MINUTE, endMinute);

        values.put(RuleTable.COLUMN_SUN, SUN ? 1 : 0);
        values.put(RuleTable.COLUMN_MON, MON ? 1 : 0);
        values.put(RuleTable.COLUMN_TUE, TUE ? 1 : 0);
        values.put(RuleTable.COLUMN_WED, WED ? 1 : 0);
        values.put(RuleTable.COLUMN_THU, THU ? 1 : 0);
        values.put(RuleTable.COLUMN_FRI, FRI ? 1 : 0);
        values.put(RuleTable.COLUMN_SAT, SAT ? 1 : 0);

        StringBuilder nameBuilder = new StringBuilder();
        StringBuilder numberBuilder = new StringBuilder();

        for(Map.Entry<ImageView, NameNumberPair> mapEntry: inMemoryContacts.entrySet()){

            Log.e(Tag,"Storing Name:" + mapEntry.getValue().name + " number: " + mapEntry.getValue().number);

            nameBuilder.append(mapEntry.getValue().name);
            nameBuilder.append(",");

            numberBuilder.append(mapEntry.getValue().number);
            numberBuilder.append(",");
        }

        values.put(RuleTable.COLUMN_FULL_NAME, nameBuilder.toString());
        values.put(RuleTable.COLUMN_NUMBER, numberBuilder.toString());

        if (ruleUri == null) {
            values.put(RuleTable.COLUMN_ISENABLED, 1);
            ruleUri = getActivity().getContentResolver().insert(RuleTableProvider.CONTENT_URI, values);
        } else {
            Log.e(Tag,"URI Not null:" + ruleUri.toString());
            getActivity().getContentResolver().update(ruleUri, values, null, null);
        }
    }

    /** let the user know that  */
    private void  makeToast(String message){
        label.setError("Cannot be Emptry");
        Toast.makeText(getActivity(), message,
                Toast.LENGTH_LONG).show();
    }



}
