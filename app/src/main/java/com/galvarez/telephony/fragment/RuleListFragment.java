package com.galvarez.telephony.fragment;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.galvarez.telephony.R;
import com.galvarez.telephony.activity.RuleActivity;
import com.galvarez.telephony.adapters.RuleCursorAdapter;
import com.galvarez.telephony.database.RuleTable;
import com.galvarez.telephony.database.RuleTableProvider;
import com.melnykov.fab.FloatingActionButton;

/**
 * A placeholder fragment containing a simple view.
 */
public class RuleListFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private final static String Tag = "RuleListFragment";

    FloatingActionButton fab;

    //Cursor stuff
    private static final int ACTIVITY_CREATE = 0;
    private static final int ACTIVITY_EDIT = 1;
    private static final int DELETE_ID = Menu.FIRST + 1;

    // private Cursor cursor;
    private RuleCursorAdapter adapter;

//    private RuleListAdapter ruleListAdapter;
    private View rootView;

    public RuleListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
        }

        fab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), RuleActivity.class));
                Toast.makeText(getActivity(),"New Rule",Toast.LENGTH_SHORT).show();
            }
        });

        fillData();
        return rootView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
        fab.attachToListView(getListView());

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.e(Tag,"onContextItemSelected()");

        switch (item.getItemId()) {
            case DELETE_ID:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                        .getMenuInfo();
                Uri uri = Uri.parse(RuleTableProvider.CONTENT_URI + "/"
                        + info.id);

                Log.e(Tag,"" + uri.toString());
                getActivity().getContentResolver().delete(uri, null, null);
                fillData();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, R.string.action_delete);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Toast.makeText(getActivity(),"Clicked ID: " + id,Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getActivity(), RuleActivity.class);
        Uri ruleUri = Uri.parse(RuleTableProvider.CONTENT_URI + "/" + id);
        i.putExtra(RuleTableProvider.CONTENT_ITEM_TYPE, ruleUri);
        startActivity(i);
    }

    public void fillData() {

        // Fields from the database (projection)
        // Must include the _id column for the adapter to work
        String[] from = new String[] {
                RuleTable.COLUMN_LABEL,
                RuleTable.COLUMN_STARTTIME_HOUR,
                RuleTable.COLUMN_STARTTIME_MINUTE,
                RuleTable.COLUMN_ENDTIME_HOUR,
                RuleTable.COLUMN_ENDTIME_MINUTE


        };
        // Fields on the UI to which we map
        int[] to = new int[] {
                R.id.tv_label_in_list,
                R.id.tv_start_hour,
                R.id.tv_start_minute,
                R.id.tv_end_hour,
                R.id.tv_end_minute
        };

        getLoaderManager().initLoader(0, null, this);
        adapter = new RuleCursorAdapter(getActivity(), R.layout.entry_rule_list, null, from,
                to, 0);
        setListAdapter(adapter);

    }

    /** This is where we set up the loader*/
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = {
                RuleTable.COLUMN_ID,
                RuleTable.COLUMN_LABEL,
                RuleTable.COLUMN_STARTTIME_HOUR,
                RuleTable.COLUMN_STARTTIME_MINUTE,
                RuleTable.COLUMN_ENDTIME_HOUR,
                RuleTable.COLUMN_ENDTIME_MINUTE,
                RuleTable.COLUMN_SUN,
                RuleTable.COLUMN_MON,
                RuleTable.COLUMN_TUE,
                RuleTable.COLUMN_WED,
                RuleTable.COLUMN_THU,
                RuleTable.COLUMN_FRI,
                RuleTable.COLUMN_SAT,
                RuleTable.COLUMN_ISENABLED

        };
        CursorLoader cursorLoader = new CursorLoader(
                getActivity(),
                RuleTableProvider.CONTENT_URI,
                projection,
                null,
                null,
                null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }




}
