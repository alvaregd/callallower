package com.galvarez.telephony.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    RuleDetailFragment.TimePickerCallBack listener;

    public void injectListener(RuleDetailFragment.TimePickerCallBack listener){
        this.listener = listener;
    }

    public static TimePickerFragment newInstance(RuleDetailFragment.TimePickerCallBack listener){
        final TimePickerFragment frag = new TimePickerFragment();
        frag.injectListener(listener);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        listener.setTime(hourOfDay,minute);
    }
}
